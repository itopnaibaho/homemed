from django import forms
from .models import Contact

class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ('Email', 'Message')

        widgets = {
            'Email' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Your email address'}),
            'Message' : forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Why did you want to work with us?'}),
        }