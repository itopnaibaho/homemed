from django.shortcuts import render, HttpResponseRedirect
from .forms import ContactForm
from .models import Contact
from django.views.generic import CreateView

def index(request):
    context ={}
  
    # create object of form
    form = ContactForm(request.POST or None)
      
    # check if form data is valid
    if (form.is_valid() and request.method == 'POST'):
        # save the form data to model
        form.save()
        form = ContactForm()
  
    context['form']= form
    return render(request, "index.html", context)

def formcoba(request):
    kontak = Contact.objects.all().values() 
    response = {'kontak': kontak}
    return render(request, 'formscoba.html', response)